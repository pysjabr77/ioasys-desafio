package br.com.ioasys.desafio.java.empresas;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class EmpresaResource {

    @RequestMapping(value = "/v1/enterprise/{id}", method = RequestMethod.GET)
    public String obter(@PathVariable("id") Long id) {
        return String.format("Ira retornar uma empresa");
    }

}
